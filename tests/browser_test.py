import genomenotebook as gn
import bokeh

def test_browser():
    p=gn.browser(genome_path="tests/data/MG1655_U00096.fasta",
          genome_format="fasta",
          gff_path="tests/data/MG1655_U00096.gff3",
          init_pos=3725353,
          init_win=10000,
         )
    assert type(p)==bokeh.plotting._figure.figure

def test_genome_browser():
    p=gn.genome_browser(genome_path="tests/data/MG1655_U00096.fasta",
          genome_format="fasta",
          gff_path="tests/data/MG1655_U00096.gff3",
          init_pos=3725353,
          init_win=10000,
         )
    assert type(p)==bokeh.models.layouts.Column