from .plotting import (
    get_genome_annotations,
    get_gene_patches, 
    get_genes_from_annotation, 
    create_genome_browser_plot,
    get_all_glyphs)
from bokeh.models import (
    CustomJS,
    Range1d,
    ColumnDataSource,
    LabelSet, 
    TextInput,
    Rect,
    Div,
    Styles
)
from bokeh.layouts import column, row

from Bio import SeqIO
from .js_callback_code import x_range_change_callback_code, search_callback_code


init_pos = 10000
init_win = 20000


def browser(genome_path: str, gff_path: str, 
             genome_format="fasta", 
             init_pos=init_pos, 
             init_win=init_win, 
             **kwargs):

    genome = next(SeqIO.parse(genome_path, genome_format))
    annotation = get_genome_annotations(gff_path)
    annotation.gene = annotation.gene.fillna("")
    annotation.gene = annotation[['gene', 'locus_tag']].apply(lambda x: f"{x[0]} {x[1]}", axis=1)
    genes = get_genes_from_annotation(annotation)

    semi_win = init_win / 2
    x_range = Range1d(
        init_pos - semi_win, init_pos + semi_win, 
        bounds=(init_pos - semi_win, init_pos + semi_win), 
        max_interval=100000,
        min_interval=50
    )
    glyph_source = ColumnDataSource(get_gene_patches(genes, x_range.start, x_range.end))
    p = create_genome_browser_plot(glyph_source, x_range, **kwargs)

    ## Adding the ability to display the sequence when zooming in
    sequence = {
        'base': list(str(genome.seq[int(x_range.start):int(x_range.end)]).upper()),
        'x': list(range(int(x_range.start), int(x_range.end)))
    }

    seq_source = ColumnDataSource({'base': [" "], 'x': [0]})
    seq_label = LabelSet(
        x='x', y=-1.98, text='base',
        text_font_size='8pt', 
        text_font_style='bold',
        source=seq_source
    )
    p.add_layout(seq_label)

    xcb = CustomJS(
        args={
            "x_range": p.x_range,
            "seq_source": seq_source,
            "sequence": sequence
        },
        code="""
            let x_size = x_range.end - x_range.start;
            const seqdata = seq_source.data;
            const x = seq_source.data['x'];
            if (x_size < 300) {
                seq_source.data['base'] = sequence['base'].slice(
                    x_range.start - sequence['x'][0], 
                    x_range.end - sequence['x'][0]
                );
                seq_source.data['x'] = sequence['x'].slice(
                    x_range.start - sequence['x'][0], 
                    x_range.end - sequence['x'][0]
                );
            } else {
                seq_source.data['base'] = [" "];
                seq_source.data['x'] = [0];
            }
            seq_source.change.emit();
        """
    )

    p.x_range.js_on_change('start', xcb)
    p.x_range.js_on_change('end', xcb)
    return p

def genome_browser(genome_path: str, gff_path: str, 
             genome_format="fasta", 
             init_pos=init_pos, 
             init_win=init_win, 
             **kwargs):
    
    max_glyph_loading_range=20000

    genome = next(SeqIO.parse(genome_path, genome_format))
    L=len(genome.seq)

    annotation = get_genome_annotations(gff_path)
    annotation.gene = annotation.gene.fillna("")
    annotation.gene = annotation[['gene', 'locus_tag']].apply(lambda x: f"{x[0]} {x[1]}", axis=1)
    genes = get_genes_from_annotation(annotation)

    semi_win = init_win / 2
    x_range = Range1d(
        max(0,init_pos - semi_win), min(L,init_pos + semi_win), 
        bounds=(0,L), 
        max_interval=100000,
        min_interval=40
    )

    glyph_source = ColumnDataSource(get_gene_patches(genes, x_range.start, x_range.end))

    #This will contain the glyphs plotted by bokeh
    loaded_glyph_source = ColumnDataSource({"start":[x_range.start],"end":[x_range.end], "range":[max_glyph_loading_range]})

    #This contains the glyphs for the whole genome
    all_glyphs=get_all_glyphs(genes, L)

    p = create_genome_browser_plot(glyph_source, x_range)

    ## Create a text input widget for search
    text_input = TextInput(value="")

    ## Adding BoxAnnotation to highlight search results
    search_span_source = ColumnDataSource({"x":[],"width":[]})#"y":[]
    h=Rect(x='x',y=-2,width='width',height=p.height,fill_color='green',fill_alpha=0.2,line_alpha=0)
    p.add_glyph(search_span_source, h)

    ## Adding the ability to display the sequence when zooming in
    sequence = {
        'seq': str(genome.seq).upper(),
    }

    sty=Styles(font_size='14px',
               font_family="Courrier",
               display="inline-block"
               )
    div = Div(height=20, height_policy="fixed", 
            width=600, width_policy="fixed",
            styles = sty)


    xcb = CustomJS(
        args={
            "x_range": p.x_range,
            "sequence": sequence,
            "all_glyphs":all_glyphs,
            "glyph_source": glyph_source,
            "div":div,
            "loaded_glyph_source":loaded_glyph_source,
        },
        code=x_range_change_callback_code
    )

    p.x_range.js_on_change('start', xcb)

    call_back_search = CustomJS(
        args={
            "x_range": p.x_range,
            "glyph_source": glyph_source,
            "sequence":sequence,
            "all_glyphs":all_glyphs,
            "text_input":text_input,
            "search_span_source":search_span_source,
            "div":div,
        },
        code=search_callback_code
    )

    text_input.js_on_change('value',call_back_search,xcb)

    return column(text_input,p,div)